# activity-files

Course resources that we want to make available to the students.


## Information confidentiality

This repository is publicly available and as such no secret, private, or
otherwise sensitive information should be stored here.

## Getting started

The repo is organized by

* module
  * week
    * day
      * activity files
      
Please make sure to get your resources into the proper directory.
